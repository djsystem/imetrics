from django.conf.urls import url, include
from django.contrib import admin
from rest_framework import routers

# Routers of api
from consumo.viewsets import UserViewSet, ServerTimeStampViewSet


router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
router.register(u'timestamp', ServerTimeStampViewSet, 'ServerTimeStamp')

urlpatterns = router.urls

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^admin/', admin.site.urls),
    url(r'^json/', include('consumo.urls'))
]
