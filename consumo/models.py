from django.db import models
from django.contrib.auth.models import User


class Usuario(User):
    dni = models.CharField(max_length=8, unique=True)
    nombres = models.CharField(max_length=16)
    apellido_paterno = models.CharField(max_length=16)
    apellido_materno = models.CharField(max_length=16)

    def __unicode__(self):
        return '{%r} {%r} {%r}'.format(self.nombres, self.apellido_paterno, self.apellido_materno)


class Tipo(models.Model):
    nombre = models.CharField(max_length=16)

    def __unicode__(self):
        return self.nombre


class Suministro(models.Model):
    momento_medicion = models.DateTimeField()
    medicion = models.DecimalField(decimal_places=2, max_digits=10)
    usuario = models.ForeignKey(User)
    tipo = models.ForeignKey(Tipo, related_name='tipo')


class Tarifa(models.Model):
    costo = models.DecimalField(decimal_places=2, max_digits=10)
    rango_minimo = models.IntegerField()
    rango_maximo = models.IntegerField(blank=True, null=True)
    tipo = models.ForeignKey(Tipo)


class Consumo(models.Model):
    suministro = models.CharField(max_length=200)
    consumo = models.CharField(max_length=200)
    fecha = models.DateTimeField(auto_now=True)

    def json_envio(self):
        return {"suministro":self.suministro,"consumo":self.consumo}

    def __str__(self):
        return str(self.id)+": "+self.suministro+" - "+self.consumo
