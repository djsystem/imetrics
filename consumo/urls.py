from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from . import viewsets
urlpatterns =[
    url(r'^consumo/(?P<suministro>[a-z0-9]+)$', viewsets.consumo, name='consumo'),
    url(r'^envio/$', viewsets.envio, name='envio'),
]
urlpatterns = format_suffix_patterns(urlpatterns)
