from django.contrib import admin
from consumo.models import *


admin.site.register(Tipo)
admin.site.register(Usuario)
admin.site.register(Consumo)
