# -*- coding: utf-8 -*-
from django.utils import timezone
from django.conf.urls import url, include
from django.contrib.auth.models import User
from rest_framework import routers, serializers, viewsets
from rest_framework.response import Response

#user
class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'is_staff')


#fecha y hora
class ServerTimeStampViewSet(viewsets.GenericViewSet):
    """
    Retorna la hora del servidor
    """
    def list(self, request):
        timestamp = timezone.now()
        return Response({'timestamp': timestamp})
