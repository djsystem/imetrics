# -*- coding: utf-8 -*-


from rest_framework.views import APIView
from django.utils import timezone
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import detail_route, list_route, api_view
from django.contrib.auth.models import User
from consumo.serializers import UserSerializer, ServerTimeStampViewSet
from consumo.models import *


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = ServerTimeStampViewSet


#ViewSet donde se pasaran el api para las tablas de contiguración
@api_view(['GET','POST'])
def consumo(request, suministro):
    print(suministro)
    print(str(request.data))
    Consumo.objects.create(suministro=suministro,consumo=str(request.data))
    return Response({"data": str(request.data)})


#ViewSet donde se pasaran el api para las tablas de contiguración
@api_view(['GET','POST'])
def envio(request):
    data = {}
    consumo=Consumo.objects.all().order_by('-fecha')[:1][0]
    return Response(consumo.json_envio())
