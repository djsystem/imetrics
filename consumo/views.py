from django.shortcuts import render
from django.views.generic import TemplateView
from django.core import serializers
from django.http import HttpResponse
from consumo.models import *



class API():

    """
    def get_suminitros(self, request):
        suministros = Suministro.objects.filter(user_id = request.GET['id']).order_by('name')
        data = serializers.serialize("json", suministros)
        return HttpResponse(data, content_type='application/json')
    """

    def get_tipos(self, request):
        tipos = Tipo.objects.all()
        data = serializers.serialize("json", tipos)
        return HttpResponse(data, content_type='application/json')
